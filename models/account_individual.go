package models

type AccountIndividual struct {
	ID                         string `json:"id"`
	AccountID                  string `json:"accountId"`
	IndividualID               string `json:"individualId"`
	Association                string `json:"association"`
	ExpectedContributionAmount int64  `json:"expectedContributionAmount"`
	ContributionStreak         int64  `json:"contributionStreak"`
	LastNotifiedStreak         int64  `json:"-"`
	CreatedAt                  int64  `json:"createdAt"`
}
