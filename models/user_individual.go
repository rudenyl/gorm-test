package models

type UserIndividual struct {
	UserID       string `json:"userId"`
	IndividualID string `json:"individualId"`
	FirstName    string `json:"firstName"`
	LastName     string `json:"lastName"`
	Role         string `json:"role"`
	CreatedAt    int64  `json:"createdAt"`
	DOB          string `json:"dob"`
}

type UserIndividualsResponse struct {
	Items []UserIndividual `json:"items"`
	Total int64            `json:"total"`
}
