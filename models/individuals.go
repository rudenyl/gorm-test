package models

type Individual struct {
	ID                         string              `json:"id"`
	UserID                     string              `json:"userId"`
	FirstName                  string              `json:"firstName"`
	LastName                   string              `json:"lastName"`
	Email                      string              `json:"email"`
	PhoneNumber                string              `json:"phoneNumber"`
	CentrelinkCRN              string              `json:"centrelinkCrn"`
	TICDonorID                 string              `json:"ticDonorId"`
	CreatedAt                  int64               `json:"createdAt"`
	Association                string              `json:"association,omitempty"`
	AccountIndividualID        string              `json:"accountIndividualId,omitempty"`
	ExpectedContributionAmount int64               `json:"expectedContributionAmount"`
	ContributionStreak         int64               `json:"contributionStreak,omitempty"`
	LastNotifiedStreak         int64               `json:"-"`
	LinkedAccounts             []AccountIndividual `json:"linkedAccounts" gorm:"foreignKey:AccountIndividualID;references:AccountID"`
}
