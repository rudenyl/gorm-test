package models

type Account struct {
	ID                         string              `json:"id"`
	AccountName                string              `json:"accountName"`
	AccountType                string              `json:"accountType"`
	AccountNumber              string              `json:"accountNumber"`
	Balance                    int64               `json:"balance"`
	CreatedAt                  int64               `json:"createdAt"`
	GoalTarget                 int64               `json:"goalTarget,omitempty"`
	Association                string              `json:"association,omitempty"`
	AccountIndividualID        string              `json:"accountIndividualId,omitempty"`
	ExpectedContributionAmount int64               `json:"expectedContributionAmount,omitempty"`
	ContributionStreak         int64               `json:"contributionStreak,omitempty"`
	LastNotifiedStreak         int64               `json:"lastNotifiedStreak,omitempty"`
	LinkedIndividuals          []AccountIndividual `json:"linkedIndividuals,omitempty" gorm:"foreignKey:AccountIndividualID;references:IndividualID"`
}
