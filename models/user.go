package models

type User struct {
	ID                           string `json:"id"`
	Email                        string `json:"email"`
	Salt                         []byte `json:"-"`
	PasswordHash                 []byte `json:"-"`
	FirstName                    string `json:"firstName"`
	LastName                     string `json:"lastName"`
	PhoneNumber                  string `json:"phoneNumber"`
	ActiveResetToken             string `json:"-"`
	Role                         string `json:"role"`
	Status                       string `json:"status"`
	StatusReason                 string `json:"-"`
	CreatedAt                    int64  `json:"createdAt"`
	DeletedAt                    int64  `json:"deletedAt"`
	LastLoginAt                  int64  `json:"lastLoginAt"`
	MiddleName                   string `json:"middleName"`
	MFAVerified                  bool   `json:"mfaVerified"`
	EmailNotificationsEnabled    bool   `json:"emailNotificationsEnabled"`
	SMSNotificationsEnabled      bool   `json:"smsNotificationsEnabled"`
	PushNotificationsEnabled     bool   `json:"pushNotificationsEnabled"`
	AcceptedTermsAndConditions   bool   `json:"acceptedTermsAndConditions"`
	LoginAttempts                int64  `json:"loginAttempts"`
	Points                       int64  `json:"points"`
	LifetimePoints               int64  `json:"lifetimePoints"`
	PreferredName                string `json:"preferredName"`
	DOB                          string `json:"dob"`
	ChildrenUnder18              string `json:"childrenUnder18"`
	EducationTrustAccount        *bool  `json:"educationTrustAccount,omitempty"`
	RelationshipStatus           string `json:"relationshipStatus"`
	Gender                       string `json:"gender"`
	Ethnicity                    string `json:"ethnicity"`
	Community                    string `json:"community"`
	EducationLevel               string `json:"educationLevel"`
	AspirationalEducationLevel   string `json:"aspirationalEducationLevel"`
	EmploymentStatus             string `json:"employmentStatus"`
	AspirationalEmploymentStatus string `json:"aspirationalEmploymentStatus"`
	AgeOfAllChildren             string `json:"ageOfAllChildren"`
	InterestedOtherTrustAcc      string `json:"interestedOtherTrustAccount"`
	InterestedHomeDeposit        *bool  `json:"interestedHomeDeposit,omitempty"`
	InterestedInSaving           string `json:"interestedInSaving"`
	IncomeSource                 string `json:"incomeSource"`
	HouseholdMakeUp              string `json:"householdMakeUp"`
	InterestedMayiMarketplace    string `json:"interestedMayiMarketplace"`
	InterestedEvents             string `json:"interestedEvents"`
	RefreshToken                 string `json:"refreshToken,omitempty"`
	RefreshTokenExpiry           int64  `json:"refreshTokenExpiry,omitempty"`
	AccessToken                  string `json:"accessToken,omitempty"`
	AccessTokenExpiry            int64  `json:"accessTokenExpiry,omitempty"`
}

type UpdateUserInput struct {
	FirstName                    string  `json:"firstName"`
	LastName                     string  `json:"lastName"`
	MiddleName                   *string `json:"middleName,omitempty"`
	PhoneNumber                  string  `json:"phoneNumber"`
	Email                        string  `json:"email"`
	PreferredName                *string `json:"preferredName,omitempty"`
	ChildrenUnder18              string  `json:"childrenUnder18"`
	RelationshipStatus           string  `json:"relationshipStatus"`
	Gender                       string  `json:"gender"`
	Ethnicity                    string  `json:"ethnicity"`
	Community                    string  `json:"community"`
	EducationLevel               string  `json:"educationLevel"`
	AspirationalEducationLevel   string  `json:"aspirationalEducationLevel"`
	EmploymentStatus             string  `json:"employmentStatus"`
	AspirationalEmploymentStatus string  `json:"aspirationalEmploymentStatus"`
	AgeOfAllChildren             string  `json:"ageOfAllChildren"`
	InterestedOtherTrustAcc      string  `json:"interestedOtherTrustAccount"`
	InterestedHomeDeposit        *bool   `json:"interestedHomeDeposit"`
	InterestedInSaving           string  `json:"interestedInSaving"`
	IncomeSource                 string  `json:"incomeSource"`
	HouseholdMakeUp              string  `json:"householdMakeUp"`
	InterestedMayiMarketplace    string  `json:"interestedMayiMarketplace"`
	InterestedEvents             string  `json:"interestedEvents"`
	Role                         string  `json:"role"`
	Status                       string  `json:"status"`
	StatusReason                 string  `json:"statusReason"`
	DOB                          string  `json:"dob"`
	EmailNotificationsEnabled    *bool   `json:"emailNotificationsEnabled,omitempty"`
	SMSNotificationsEnabled      *bool   `json:"smsNotificationsEnabled,omitempty"`
	PushNotificationsEnabled     *bool   `json:"pushNotificationsEnabled,omitempty"`
	AcceptedTermsAndConditions   *bool   `json:"acceptedTermsAndConditions,omitempty"`
}

func (d DB) GetUser(id string) (User, error) {
	var user User

	result := d.DB.Table("pamauser").First(&user, User{ID: id})

	return user, result.Error
}
